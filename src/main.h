#pragma once
/*
  Configuration values of the modes
*/
float config_current_value = 0;
float config_voltage_value = 0;

#define BUTTON_UP_PIN 3
#define BUTTON_SELECT_PIN 7
#define BUTTON_DOWN_PIN 11


/*
  define the JC Buttons
*/
  Button btnUp = Button(BUTTON_UP_PIN);
  Button btnDown = Button(BUTTON_DOWN_PIN);
  Button btnSelect = Button(BUTTON_SELECT_PIN);

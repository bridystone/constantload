#pragma once

#include <Arduino.h>
#include <JC_Button.h>
#include <LiquidCrystal_I2C.h>
#include "timer.h"
#include "sense.h"


enum menu_modes {MAIN, CURRENT, VOLTAGE, CURRENT_SET, VOLTAGE_SET, BATTERY_SET};



class Menu {
  float config_current_value, config_voltage_value;
  byte current_menu_position = 0;
  bool menu_changed = true;
  menu_modes current_menu_mode = menu_modes::MAIN;


public:
  Menu(Timer timer, Sense sense);
  void setup_menu();
  void execute_menu(bool btnUp, bool btnDown, bool btnSelect);

private:

  // Set the LCD address to 0x27 for a 16 chars and 2 line display
  LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27,16,2);
  Timer _timer;
  Sense _sense;
  bool _btnUp, _btnDown, _btnSelect;

  void execute_main_menu();
  void execute_voltage_set_menu();
  void execute_current_set_menu();
  void execute_voltage_menu();
  void execute_current_menu();
  void execute_battery_set_menu();

  void print_set_menu(const char menu_name[], const char unit[], float value);
  bool execute_set_buttons(float *value, menu_modes next_menu_mode);

};
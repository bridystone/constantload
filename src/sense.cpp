  #include "sense.h"

  void Sense::begin(byte current_sensor_pin, byte voltage_sensor_pin)
  {
    _current_sensor_pin = current_sensor_pin;
    _voltage_sensor_pin = voltage_sensor_pin;

    _current_ground_level = getVoltage(_current_sensor_pin);
  }

  float Sense::getCurrent() {
    /*
    vals = (vals/_accuracy) - _current_ground_level;
    vals = vals * (_reference_voltage/1023) / _current_sensor_sensitivity;
*/
    return getVoltage(_current_sensor_pin);
  }


/***************************
 * available voltage levels
 * and their dividers in mV
 * *************************/
struct adc_voltage_levels_t {
    VREF_ADC0REFSEL_enum voltage_level;
    float divider;
  };

adc_voltage_levels_t adc_voltage_levels[4] = {
//  {VREF_ADC0REFSEL_0V55_gc, 550},
  {VREF_ADC0REFSEL_1V1_gc, 1100},
  {VREF_ADC0REFSEL_1V5_gc, 1500},
  {VREF_ADC0REFSEL_2V5_gc, 2500},
  {VREF_ADC0REFSEL_4V34_gc, 4240}
};

float Sense::getVoltage(byte pin) {
  int result;
  int i = 0;
  // cycle through all available ref voltage levels starting from bottom
  for (i = 0; i<sizeof(adc_voltage_levels)/sizeof(adc_voltage_levels_t); i++) {
    result = analogReadADC(pin, adc_voltage_levels[i].voltage_level);
    // get minimal successful analogRead
    if (result<1024) {
      break;
    }
  }

  return result * adc_voltage_levels[i].divider/1023.0; // maximum voltage in mV (4340) / maximum level => 10bit = 1023
}

float Sense::getVoltage() {
  return getVoltage(_voltage_sensor_pin);
}

int Sense::analogReadADC(
  pin_size_t pin, 
  VREF_ADC0REFSEL_enum voltage_reference = VREF_ADC0REFSEL_enum::VREF_ADC0REFSEL_4V34_gc,
  ADC_SAMPNUM_enum number_of_samples = ADC_SAMPNUM_enum::ADC_SAMPNUM_ACC64_gc,
  ADC_RESSEL_enum adc_resolution = ADC_RESSEL_enum::ADC_RESSEL_10BIT_gc
  )
{

  // get analog input from pin number
  int analogInput = digitalPinToAnalogInput(pin);
  if(analogInput > NUM_ANALOG_INPUTS) return NOT_A_PIN;  

  // set the reference Value ==> 4,34 == 1023 (0xF => do not touch the AC configuration)
  // enforce enablement/usage of reference voltage ==> no auto mode
  VREF_CTRLA = voltage_reference | 0xF;
  VREF_CTRLB |= VREF_ADC0REFEN_bm;

/***********************
 * Configur ADC
 * *********************/
  // set resolution to 10bit
  ADC0_CTRLA = adc_resolution;

  // define number of samples to take 
  ADC0_CTRLB = number_of_samples;


  // set sample capacitance (if expexted samples are lower than 1V -> low sample capacitance)
  // set usage of internal reference
  ADC0_CTRLC = ( 
                (voltage_reference == VREF_ADC0REFSEL_0V55_gc) ||
                (voltage_reference == VREF_AC0REFSEL_1V1_gc)
              )?ADC_SAMPCAP_bm:~ADC_SAMPCAP_bm |
              // set internal reference
              ADC_REFSEL_enum::ADC_REFSEL_INTREF_gc |
              // set PRESCALER CLK/2 (default ==> 8bit)
              // 10bit => 5kHz & 1.5MHz 16Mhz => CLK/256 = 62500kHz CLK/16 = 1MHz
              // 8Bit  >= 1.5MHz
              (adc_resolution == ADC_RESSEL_enum::ADC_RESSEL_10BIT_gc)?
                    ADC_PRESC_enum::ADC_PRESC_DIV16_gc : ADC_PRESC_enum::ADC_PRESC_DIV2_gc;
              
  // dutycycle 50% when ADC CLK > 1,5Mhz (8bit ADC) -> otherwise (10bit) set 25% duty cycle ==> see PRESCALER
  ADC0_CALIB = (adc_resolution == ADC_RESSEL_enum::ADC_RESSEL_10BIT_gc)?
                    ADC_DUTYCYC_enum::ADC_DUTYCYC_DUTY25_gc:ADC_DUTYCYC_enum::ADC_DUTYCYC_DUTY50_gc;

  // no delay (default)
  // automatic delay variation in sampling
  ADC0_CTRLD = ADC_INITDLY_enum::ADC_INITDLY_DLY0_gc | ADC_ASDV_enum::ADC_ASDV_ASVON_gc;

/**********************
 * configure port 
 * ********************/
  // Port Configuration

  // DISABLE INPUTBUFFER & INTERRUPT FOR PIN PD0 == A3
  // i.e. A3 ==> PORTD->PIN0CTRL
  /* Calculate where pin control register is */
  PORT_t* port = digitalPinToPortStruct(pin);
  if(port == NULL) return;
  uint8_t bit_pos = digitalPinToBitPosition(pin);
  volatile uint8_t* pin_ctrl_reg = getPINnCTRLregister(port, bit_pos);
  port->DIR &= ~bit_pos; //explicitely define bit as INPUT

  // DISABLE INPUTBUFFER & INTERRUPT FOR PIN 
  *pin_ctrl_reg = PORT_ISC_INPUT_DISABLE_gc;


  // SET the OUTPUT to AIN0 
  ADC0_MUXPOS = analogInput;

/**************************
 * perform the actual read 
 **************************/
  // activate ADC
  ADC0_CTRLA |= ADC_ENABLE_bm;

  // execute conversion
  ADC0_COMMAND |= ADC_STCONV_bm;
  // wait until conversion is finished
  while (!(ADC0_INTFLAGS & ADC_RESRDY_bm));

  // receive result
  unsigned int result = ADC0_RES;
  // disable ADC
  ADC0_CTRLA &= ~ADC_ENABLE_bm;


/**************************
 * aftermatch 
 * ************************/
  // divide result by number of taken samples
  result /= pow(2, number_of_samples);


  // Return the conversion result
  return( result );
}

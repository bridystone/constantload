#pragma once;
#include <Arduino.h>

class Sense {
  byte _current_sensor_pin;
  int _current_ground_level;
  byte _voltage_sensor_pin;

/**
 * Sensitivity of Current sensor
 *  5A = 185.00
 * 20A = 100.00
 * 30A = 66.00;
 */
float _current_sensor_sensitivity = 100.00;

public:
  void begin(byte current_sensor_pin, byte voltage_sensor_pin);
  float getCurrent();
  float getVoltage();
  float getVoltage(byte pin);

private:
  int analogReadADC(
    pin_size_t pin, 
    VREF_ADC0REFSEL_enum voltage_reference = VREF_ADC0REFSEL_enum::VREF_ADC0REFSEL_4V34_gc,
    ADC_SAMPNUM_enum number_of_samples = ADC_SAMPNUM_enum::ADC_SAMPNUM_ACC64_gc,
    ADC_RESSEL_enum adc_resolution = ADC_RESSEL_enum::ADC_RESSEL_10BIT_gc
  );
};
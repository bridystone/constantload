
#include <Arduino.h>
#include <JC_Button.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#include "main.h"
#include "menu.h"
#include "timer.h"
#include "sense.h"

#define CURRENT_SENSOR_PIN A7
#define VOLTAGE_SENSOR_PIN A6


Timer timer;
Sense sense;
Menu menu = Menu(timer, sense);

void setup()
{
  Serial.begin(9600);

  /*
    init buttons
  */
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_SELECT_PIN, INPUT_PULLUP);

  btnUp.begin();
  btnDown.begin();
  btnSelect.begin();

  /* to be placed in the constructor */
  sense.begin(CURRENT_SENSOR_PIN, VOLTAGE_SENSOR_PIN);

	// initialize the LCD 
  menu.setup_menu();


  // prepare Timer & set duty cycle to 0
  // PWM PIN = 9
  timer.init_timer();
}


void loop()
{
  btnDown.read();
  btnUp.read();
  btnSelect.read();

  menu.execute_menu(btnUp.wasPressed(), btnDown.wasPressed(), btnSelect.wasPressed());
}



#include <LiquidCrystal_I2C.h>
#include <Adafruit_INA219.h>

#include "menu.h"

Adafruit_INA219 ina219_monitor;


Menu::Menu(Timer timer, Sense sense) {
  _timer = timer;
  _sense = sense;
}

void Menu::setup_menu() {

  lcd.begin();

  // Turn on the blacklight and print a message.
  lcd.backlight();
  lcd.print("Hello, world!");

  if (! ina219_monitor.begin()) {
    lcd.clear();
    lcd.print("Failed to find INA219 chip");
    while (1) { delay(10); } // stop operations
  }
  // low sensitivity
  ina219_monitor.setCalibration_32V_2A();
}

void Menu::execute_menu(bool btnUp, bool btnDown, bool btnSelect) {

  _btnUp = btnUp;
  _btnDown = btnDown;
  _btnSelect = btnSelect;

  switch (current_menu_mode) {
    case menu_modes::MAIN :
      execute_main_menu();
      break;
    case menu_modes::CURRENT_SET :
      execute_current_set_menu();
      break;
    case menu_modes::CURRENT :
      execute_current_menu();
      break;
    case menu_modes::VOLTAGE_SET :
      execute_voltage_set_menu();
      break;
    case menu_modes::VOLTAGE :
      execute_voltage_menu();
      break;
    case menu_modes::BATTERY_SET :
      execute_battery_set_menu();
      break;
    default:
      break;
  };
}

void Menu::execute_main_menu() {
  if (menu_changed) {
    lcd.clear();
    lcd.print((current_menu_position==0)?">":" ");
    lcd.print("CURRENT|");
    lcd.print((current_menu_position==1)?">":" ");
    lcd.print("VOLTAGE");
    lcd.setCursor(0,1);
    lcd.print((current_menu_position==2)?">":" ");
    lcd.print("3,7V CC/CV");

    menu_changed = false;
  }

  if (_btnUp) {
    current_menu_position+=(current_menu_position==2)?0:1;
    menu_changed = true;
  }
  if (_btnDown) {
    current_menu_position-=(current_menu_position==0)?0:1;
    menu_changed = true;
  }
  if (_btnSelect) {
    // switch to new menu
    switch (current_menu_position)
    {
    case 0:
      current_menu_mode = menu_modes::CURRENT_SET; 
      break;
    case 1:
      current_menu_mode = menu_modes::VOLTAGE_SET; 
      break;
    case 2:
      current_menu_mode = menu_modes::BATTERY_SET; 
      break;
    
    default:
      break;
    }
    current_menu_position = 0;
    menu_changed = true;
  }
}

void Menu::print_set_menu(const char menu_name[], const char unit[], float value) {
    lcd.clear();
    lcd.print((current_menu_position==0)?">":" ");
    lcd.print(menu_name);
    lcd.print(": ");
    lcd.print(value);
    lcd.print(unit);
    lcd.setCursor(0,1);
    lcd.print((current_menu_position==1)?">":" ");
    lcd.print("START ");
    lcd.print((current_menu_position==2)?">":" ");
    lcd.print("BACK");

}

/* returns true, if SELECT was pushed and operations should start */
bool Menu::execute_set_buttons(float *value, menu_modes next_menu_mode) {
  switch (current_menu_position) {
    // set value
    case 0:
      if (_btnUp) {
        *value += 0.1;
        menu_changed = true;
      }
      if (_btnDown) {
        *value -= 0.1;
        menu_changed = true;
      }
      // set the value and choose start or back
      if (_btnSelect) {
        current_menu_position = 1;
        menu_changed = true;
      }
      break;
      // start or set
    default:
      if (_btnUp) {
        current_menu_position = 1;
        menu_changed = true;
      }
      if (_btnDown) {
        current_menu_position = 2;
        menu_changed = true;
      }
      if (_btnSelect) {
        current_menu_mode = (current_menu_position == 1)? next_menu_mode : menu_modes::MAIN;
        current_menu_position = 0;
        menu_changed = true;
      }
      break;
  }

  // is the operation supposed to start ?*/
  return current_menu_mode == next_menu_mode;
}

/* 
  SET Menu Stuff
*/
void Menu::execute_current_set_menu(){
  if (menu_changed) {
    print_set_menu("CURRENT", "A", config_current_value);
    // set start value to decent start
    _timer.set_duty_cycle(1500);
    menu_changed = false;
  }
  execute_set_buttons(&config_current_value, menu_modes::CURRENT);
}

void Menu::execute_voltage_set_menu() {
  if (menu_changed) {
    print_set_menu("VOLTAGE", "V", config_voltage_value);
    menu_changed = false;
  }
  execute_set_buttons(&config_voltage_value, menu_modes::VOLTAGE);
}


/* Voltage divider specifications */
float _resistor_top = 10000.0; // resistance of upper resistor - see above Graphics
float _resistor_bottom = 2200.0; // resistance of lower resistor - see above Graphics
float _voltage_divider_ratio = _resistor_bottom / (_resistor_top + _resistor_bottom);

unsigned long last_millis = 0;

void Menu::execute_current_menu(){
  if (menu_changed) {
    lcd.clear();
    lcd.println("EXECUTING CURRENT");
    menu_changed = false;
  }
  lcd.setCursor(0,1);
  //ACS712
  //float actual_current = _sense.getCurrent();
  float actual_current = ina219_monitor.getCurrent_mA();
  float actual_voltage = _sense.getVoltage() / _voltage_divider_ratio / 1000;

  if (millis()>last_millis) {
    // only print every second
    last_millis = millis()+10000;
  //  lcd.print(actual_voltage);
    lcd.print(actual_current);
    lcd.print("|");
    lcd.print(_timer.get_duty_cycle());
  /*  lcd.print(actual_current);
    lcd.print("mA|");
    lcd.print(actual_voltage);
    lcd.print("V");
  */
    Serial.print(_timer.get_duty_cycle());
    Serial.print(":");
    Serial.print(actual_current);
    Serial.print("mA:");
    Serial.print(config_current_value);
    Serial.print("mA:");
    Serial.print(actual_voltage);
    Serial.println("V");
  }
  if (actual_current < config_current_value) {
    _timer.increment_duty_cycle();
  } else {
    _timer.decrement_duty_cycle();
  }

 if (_btnUp) {
   _timer.set_duty_cycle(_timer.get_duty_cycle()+200);
 }
 if (_btnDown) {
   _timer.set_duty_cycle(_timer.get_duty_cycle()-200);
 }
  delayMicroseconds(20);

  // terminate operations
  if (_btnSelect) {
    current_menu_mode = menu_modes::MAIN;
    current_menu_position = 0;

    _timer.shutdown();
    menu_changed = true;
  }
}

void Menu::execute_voltage_menu(){
  if (menu_changed) {
    lcd.clear();
    lcd.println("EXECUTING VOLTAGE");
    menu_changed = false;
  }
  if (_btnSelect) {
    current_menu_mode = menu_modes::MAIN;
    current_menu_position = 0;
    menu_changed = true;
  }
}


/* ----------------- BATTERY ------------- */
void Menu::execute_battery_set_menu(){
  if (menu_changed) {
    lcd.clear();
    lcd.println("BATTERY: TODO");
    menu_changed = false;
  }
  if (_btnSelect) {
    current_menu_mode = menu_modes::MAIN;
    current_menu_position = 0;
    menu_changed = true;
  }
}

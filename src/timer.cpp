#include <Arduino.h>
#include "timer.h"

#define PERIOD  2047

void Timer::init_timer() {
  /* PREPARE THE TIMER */
 /* 
 Timer set for PWM
 */

  noInterrupts();

  /* Code for Uno & Nano */
/*
//  TCCR1A = 10000010 (COM1A1 & WGM11[WGM10=0])
//  TCCR1B = 00011001 (WGM13, WGM12, CS10)
// ==> WGM = 1110 ==> Fast PWM, TOP:ICR1, Update of OCR1:BOTTOM, TOV1Flag Set: TOP
// ==> CS = 001 ==> no prescaling
// ==> COM1A/B = 1000 => Clear OC1A on Compare Match (Set output to low level).
  TCCR1A = (1 << COM1A1) | (1 << WGM11);
  TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS10);

  // set TOP & duty cycle
  ICR1 = 2047;
// set counter to 0
  OCR1A = 0;
*/
/*
  Code for nano Every MegaAVR 
*/
  // no prescaling & enable TCA
  TCA0_SINGLE_CTRLA = TCA_SINGLE_CLKSEL1_bm |TCA_SINGLE_ENABLE_bm;
  // choose mode of operations = 3 => Single Slope PWM
  TCA0_SINGLE_CTRLB = TCA_SINGLE_CMP0EN_bm | TCA_SINGLE_WGMODE1_bm | TCA_SINGLE_WGMODE0_bm;

  // set TOP & duty cycle
  TCA0_SINGLE_CMP0BUF = 0; // start variable value of duty cycle (start disabled)
  TCA0_SINGLE_PERBUF = PERIOD; // PER = period = TOP

  // actively disable "Count on Event input" which is default
  // from example: TB3217-Getting-Started-with-TCA-90003217A (documents)
  // TCA0.SINGLE.EVCTRL &= ~(TCA_SINGLE_CNTEI_bm);  

  // Specify output port
  // TCAPIN: Pin 9 ==> Port PB0 ==> Portmux:B, DIR:0 (see pinout)
  PORTMUX.TCAROUTEA &= B11111000;
  PORTMUX.TCAROUTEA |= PORTMUX_TCA0_PORTB_gc; 
  PORTB_DIR |= PIN0_bm;

  interrupts();
}
void Timer::increment_duty_cycle() {
  noInterrupts();
  // check if duty cycle is already at maximum
  if (TCA0_SINGLE_CMP0BUF >= PERIOD) {
    TCA0_SINGLE_CMP0BUF = PERIOD;
  } else {
    TCA0_SINGLE_CMP0BUF++;
  }
  interrupts();
  delay(1000);
}

void Timer::decrement_duty_cycle() {
  noInterrupts();
  // check if duty cycle is already at minimum
  if (TCA0_SINGLE_CMP0BUF <= 0) {
    TCA0_SINGLE_CMP0BUF = 0;
  } else {
    TCA0_SINGLE_CMP0BUF--;
  }
  interrupts();
  delay(1000);
}

int Timer::get_duty_cycle() {
  return TCA0_SINGLE_CMP0BUF;
}

void Timer::set_duty_cycle(int duty_cycle) {
  TCA0_SINGLE_CMP0BUF = duty_cycle;
}

void Timer::shutdown() {
  noInterrupts();
  TCA0_SINGLE_CMP0BUF = 0;
  interrupts();
}
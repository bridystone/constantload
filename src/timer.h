#pragma once;
class Timer {
public:
  void init_timer();
  void increment_duty_cycle();
  void decrement_duty_cycle();
  int get_duty_cycle();
  void set_duty_cycle(int duty_cycle);
  void shutdown();
};